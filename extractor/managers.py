# -*- coding: utf-8 -*-
import os, json

from extractors import *
from register import EXTRACTORS
from solr import GeneratorSolrXML

class ManagerDocs(object):

    def __init__(self, path_in_files, path_out_files, limit_infile, file_name, schema=None):
        self.path_in_files = path_in_files
        self.path_out_files = path_out_files
        self.limit_infile = limit_infile
        self.file_name = file_name
        self.schema = schema

    def generateSolrXML(self):

        self.generator = GeneratorSolrXML(
                        limit_docs_in_file=self.limit_infile,
                        path_save_xml=self.path_out_files,
                        xml_filename=self.file_name)

        files = self.getListInFiles()
        for file in files:
            doc = self.extractDoc(file)
            self.generator.generateDoc(doc)

        #caso não tenha completado um ciclo minimo de documentos salva        
        self.generator.save_xml()


    def extractDoc(self, file):

        manager_doc = ManagerXMLDoc()
        manager_doc.path_xml = self.path_in_files
        if self.schema:
            manager_doc.source_schema_json = self.schema
        else:
            manager_doc.source_schema_json = 'doc_schema.json'

        doc = manager_doc.getList(file)

        return doc


    def getListInFiles(self):
        list = os.listdir(self.path_in_files)
        return list

class ManagerXMLDoc(object):
    """aqui extrai todos os itens do schema"""

    #diretorio raiz de arquivo de schema
    path_config = os.path.dirname(__file__)+'/'
    #arquivo do esquema de extração
    source_schema_json = 'doc_schema.json'
    #json com schema carregado
    schema = None
    #path absoluto com arquivo xml de entrada
    path_xml = ''

    def loadSchema(self):
        """ Metodo carrega arquivo de schema """
        if self.source_schema_json:
            if not '/' in self.source_schema_json:
                file = os.path.join(self.path_config, self.source_schema_json)
            else:
                file = self.source_schema_json

            with open(file) as schema_json:
                self.schema = json.load(schema_json)


    def getList(self, xml_file):
        """Metodo retorna lista de tuplas com campos extraidos do arquivo xml"""
        self.loadSchema()
        absolute_xml_path = self.path_xml +'/'+ xml_file
        generator = GeneratorListXML(xml_path=absolute_xml_path, file_name=xml_file)

        lista = generator.generateExtractionList(self.schema)
        return lista

    def showExtractors(self):
        """ metodo apresenta dicionario de extratores registrados """
        print EXTRACTORS


class GeneratorListXML(object):

    def __init__(self, **kwargs):
        if not 'xml_path' in kwargs:
            raise ValueError('xml_path parameter is required!')

        for key,value in kwargs.iteritems():
            #cria variveis de todos os atributos passados com chave valor
             setattr(self, key, value)

    def getExtractor(self, name, **kwargs):
        """Metodo executa extractor e retorna valor extraido"""
        extractor = EXTRACTORS[name](**kwargs)
        return extractor

    def generateField(self, field_name, xpath, attrib, name_extractor):
        """ metodo retorna chave valor de valor extraido da classe extractora """
        extractor = self.getExtractor(name=name_extractor, xpath=xpath, attribute=attrib, xml=self.xml_path, field_name=field_name)

        result = extractor.getExtraction()

        return result


    def generateExtractionList(self, schema):
        """ metodo gera lista de tuplas com fields que seram inseridos no arquivo final """
        gen_list = []
        # itera todos os fields configurados no schema
        for field in schema['fields']:

            field_extract = self.generateField(field['field_name'], field['xPath'], field['attrib'], field['extractor'])

            #se extrator voltar uma lista adiciona item a item, caso negativo inclui a tupla diretamente
            if isinstance(field_extract, list):
                for fd in field_extract:
                    if isinstance(fd, tuple):
                        gen_list.append(fd)

                    elif isinstance(fd, list):
                        for f in fd:
                            if isinstance(f, tuple):                                
                                gen_list.append(f)
                    else:
                        raise ValueError("Item da lista deveria ser do tipo tuple, mas é um(a) %s"% type(fd))
            elif isinstance(field_extract, tuple):
                gen_list.append(field_extract)
            else:
                raise ValueError("Item da lista deveria ser do tipo tuple ou ser uma lista de tuplas, mas é um(a) %s"% type(field_extract))

        return gen_list
