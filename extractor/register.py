# -*- coding: utf-8 -*-
""" Script contem codigo para registro de classes extractor """


EXTRACTORS = {}

""" Registra classe em icio0nario utilizado
    no fluxo de processamento da extração"""

class RegistryExtractor(object):

    def __init__(self, name):
        self.name = name

    def __call__(self, cls, *args, **kwargs):
        EXTRACTORS[self.name] = cls
        return cls
