# -*- coding: utf-8 -*-
from register import RegistryExtractor
from extract import ExtractXMLField, ExtractXMLFieldNumerated, ExtractXMLFieldsExtructuredAttributes


#registra classe para disponibilizar no processamento do schema
#name é utilizado no parametro extractor do schema
@RegistryExtractor(name='GetAtributes')
class GetAtributes(ExtractXMLField):
    """ classe extrai atributos de elemento na sequencia
    definida em configuração para formar uma estrutura de dados de facet"""

    def setReplace(self, string):
        """Sobreescreve metodo com replace em valores extraidos"""
        resultado = string
        if not isinstance(string, basestring) and not string is None:
            string = str(string)
            resultado = string.replace('|', '')

        return resultado

    def extractValue(self):
        """metodo deve retornar uma tupla ou lista de tuplas"""
        #pega lista de resultados do parametro xpath no arquivo xml
        xpath_result = self.getXpathLxml()

        value = self.getAtribute(self.attribute, xpath_result)

        if len(value) > 1:
            result = self.tryListToMultipleFields(value)
        elif len(value) == 1:
            result = (self.field_name, value[0])
        else:
            result = (self.field_name, None)

        return result

@RegistryExtractor(name='GetAtributesExtructured')
class GetAtributesExtructured(ExtractXMLFieldsExtructuredAttributes):
    """ classe extrai atributos de elemento na sequencia
    definida em configuração para formar uma estrutura de dados de facet"""

    def extractValue(self):
        """metodo deve retornar uma tupla ou lista de tuplas"""
        #pega lista de resultados do parametro xpath no arquivo xml
        xpath_result = self.getXpathLxml()

        value = self.iterXpathToFields(xpath_result)

        if len(value):
            return value
        else:
            return (self.field_name, None)

#registra classe para disponibilizar no processamento do schema
@RegistryExtractor(name='RootAttribute')
class RootAttribute(ExtractXMLField):
    """ classe extrai atributo do elemento root do arquivo xml"""
    def extractValue(self):

        xpath_result = self.getXpathLxml()

        value = self.getAtribute(self.attribute, xpath_result)

        if len(value) == 1:
            result = (self.field_name, value[0])
        else:
            result = (self.field_name, None)

        return result

@RegistryExtractor(name='FileName')
class ExtractFileName(ExtractXMLField):
    """ classe extrai nome do arquivo de entrada xml"""
    def extractValue(self):

        value = self.xml
        value = value.split('/')[-1]
        value = value.replace('.xml','')

        result = (self.field_name, value)

        return result

@RegistryExtractor(name='CounterElements')
class CounterElements(ExtractXMLField):
    """ classe extrai nome do arquivo de entrada xml"""
    def extractValue(self):

        lista = self.getXpathLxml()
        value = len(lista)

        result = (self.field_name, str(value))

        return result

@RegistryExtractor(name='FacetAtributes')
class FacetGetAtributes(ExtractXMLField):
    """ classe extrai atributos de elemento na sequencia
    definida em configuração para formar uma estrutura de dados de facet"""

    #sobrescreve metodo
    def getAtribute(self,atributes, xpath_result):
        """metodo retorna lista de valores concatenados em pipe encontrado em atributos informados"""
        result = []
        for element in xpath_result:
            value = []
            contador = 0
            for attr in atributes:
                if attr == 'text':
                    value.append(element.text)
                else:
                    atributo = element.get(attr)
                    if len(value) > 1:
                        if not value[1] and contador == 2 and atributo:
                            value[1] = u'Não identificado'

                    value.append(atributo)

                contador += 1

            if value:
                value = [i for i in value if i]

                result.append('|'.join(value))

        return result

    def extractValue(self):
        """metodo deve retornar uma tupla ou lista de tuplas"""
        #pega lista de resultados do parametro xpath no arquivo xml
        xpath_result = self.getXpathLxml()

        value = self.getAtribute(self.attribute, xpath_result)


        if len(value) > 1:
            result = self.tryListToMultipleFields(value)
        elif len(value) == 1:
            result = (self.field_name, value[0])
        else:
            result = (self.field_name, None)

        return result

@RegistryExtractor(name='MultipleAtributes')
class GetMultipleAtributesInOne(ExtractXMLField):
    """ classe extrai multiplos atributos para um unico field de um elemento"""

    def extractValue(self):
        """metodo deve retornar uma tupla ou lista de tuplas"""
        #pega lista de resultados do parametro xpath no arquivo xml
        xpath_result = self.getXpathLxml()

        value = []
        for attr in self.attribute:
            value += self.getAtribute(attr, xpath_result)


        if len(value) > 1:
            result = self.tryListToMultipleFields(value)
        elif len(value) == 1:
            result = (self.field_name, value[0])
        else:
            result = (self.field_name, None)

        return result
