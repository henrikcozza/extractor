# -*- coding: utf-8 -*-
from lxml import etree

class ExtractXMLField(object):
    """Classe abstrata para extração de valores de arquivos xml"""

    xml = ''
    xpath = '/'
    attribute = []
    field_name = ''

    def __init__(self, **kwargs):
        for key,value in kwargs.iteritems():
            #cria variveis de todos os atributos passados com chave valor
             setattr(self, key, value)

    def getExtraction(self):
        """ retorna tupla ou lista de tuplas dos valores extraidos,
        com o formato (field_name, value) """

        value = self.extractValue()
        return value

    def loadXML(self):
        if self.xml:
            tree = etree.parse(self.xml)
            return tree
        else:
            raise ValueError("xml not defined")

    def getXpathLxml(self):
        """Extrai lista de elementos do xml"""
        if self.xpath:

            xml = self.loadXML()
            find = etree.XPath(self.xpath)
            result = find(xml)
            return result
        else:
            raise ValueError("xpath not defined")

    def extractValue(self):
        """ Interface method """
        raise NotImplementedError( "Class %s doesn't implement extractValue()" % (self.__class__.__name__) )

    def tryListToMultipleFields(self, list_value):
        """ metodo transforma lista de valor em uma lista de tuplas """
        #usar set(list) para remover duplicatas identicas
        list_tuple = []
        for value in list_value:
            list_tuple.append( (self.field_name, value) )
        return list(list_tuple)

    def setReplace(self, string):
        """metodo que substitui caracter de valores deve ser sobreescrito para modificações"""
        return string

    def getAtribute(self,atribute, xpath_result):
        """metodo retorna lista de valores encontrados em atributo informado"""
        result = []
        for element in xpath_result:
            if atribute == 'text':
                value = element.text
            else:
                value = element.get(atribute)
            #replace string com metodo sobreescrito
            value = self.setReplace(value)
            if value:
                result.append(value)

        return result


class ExtractXMLFieldNumerated(ExtractXMLField):

    def tryListToMultipleFields(self, list_value):
        """ metodo transforma lista de valor em uma lista de tuplas """
        #usar set(list) para remover duplicatas identicas
        list_tuple = []
        contador = 0
        for value in list_value:
            contador += 1
            list_tuple.append( (self.field_name+'_'+str(contador), value) )
        return list(list_tuple)


class ExtractXMLFieldsExtructuredAttributes(ExtractXMLField):

    def iterXpathToFields(self, xpath_result):
        """Itera lista de objetos xml e retorna lista de tuplas com os campos extraidos"""

        list_tuples = []
        if isinstance(xpath_result, list):
            contador = 1
            for xpath in xpath_result:
                list_tuples.append( self.getAtributes(self.attribute, xpath, contador) )
                contador +=1
        return list_tuples

    def getAtributes(self,attribute, xpath_result, id_estruct=''):
        """metodo retorna lista estruturada de valores encontrados nos atributos informados"""
        result = []

        #basestring é uma superclasse abstrata de string e unicode, porem não existe no python 3
        if not isinstance(id_estruct, basestring):
            id_estruct = str(id_estruct)

        if isinstance(attribute, list):

            for attr in attribute:
                #precisa ser lista para getAtribute
                xp = [xpath_result]

                value = self.getAtribute(attr, xp)

                field_name = self.field_name + '_' + id_estruct + '_' + attr

                if len(value) > 1:
                    result_attr = (field_name, self.tryListToMultipleFields(value))

                elif len(value) == 1:
                    result_attr = (field_name, value[0])

                else:
                    result_attr = (field_name, None)


                result.append(result_attr)

        return result
