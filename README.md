# Extractor
Modulo multipropósito para extração de dados de um conjunto de arquivos, formatando os em arquivos para carga de dados em servidores Solr/Lucene


![Cat](doc/media/ExtractorConcept.png)


### Exemplo
```python

from .extractor.managers import ManagerXMLDoc, ManagerDocs

manager = ManagerDocs(
            path_in_files = '/abs_path/to/data_source/',
            path_out_files = '/abs_path/to/dir_out',
            limit_infile = 100,
            file_name = 'solr_lattes_100',
            schema= '/abs_path/to/schema.json')

manager.generateSolrXML()

```